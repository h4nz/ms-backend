package com.gtech.Util.GenerateNumber;

import com.fasterxml.uuid.Generators;

import java.util.UUID;

public class UniquiNumber {

    public static String generateNumber12(){
        UUID uuid = Generators.timeBasedGenerator().generate();
        String number12 = uuid.toString().substring(uuid.toString().length() -12);
        return number12;
    }
}
