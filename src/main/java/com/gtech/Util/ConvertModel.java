package com.gtech.Util;

import org.modelmapper.ModelMapper;

public interface ConvertModel {
    default <T> T converTo(Class<T> targetClass){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(this,targetClass);
    }
}
