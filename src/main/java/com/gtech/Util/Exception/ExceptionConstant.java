package com.gtech.Util.Exception;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ExceptionConstant {

    public final class CommonErrorCode{
        public static final String GENERAL_ERROR_CODE="500";
        public static final String SUCCESS_NO_CONTENT="204";

        CommonErrorCode(){
        }
    }

    public static final Map<String, String> CommonErrorDesc;

    static {
        HashMap<String,String> desc = new HashMap<>();
        desc.put(CommonErrorCode.GENERAL_ERROR_CODE,"General Error.");
        desc.put(CommonErrorCode.SUCCESS_NO_CONTENT,"The server successfully processed the request, and is not returning any content'");

        CommonErrorDesc = Collections.unmodifiableMap(desc);
    }


}
