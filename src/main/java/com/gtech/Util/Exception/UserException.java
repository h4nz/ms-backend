package com.gtech.Util.Exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserException extends Exception {
    String errorCode;
    String errorDesc;

    public UserException(String errorCode,String errorDesc){
        this.errorCode=errorCode;
        this.errorDesc=errorDesc;
    }

}
