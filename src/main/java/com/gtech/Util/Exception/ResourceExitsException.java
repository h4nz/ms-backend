package com.gtech.Util.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class ResourceExitsException extends RuntimeException {
    public ResourceExitsException(String message) {
        super(message);
    }

    public ResourceExitsException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
