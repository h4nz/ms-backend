package com.gtech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class MsBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsBackendApplication.class, args);
	}

}
