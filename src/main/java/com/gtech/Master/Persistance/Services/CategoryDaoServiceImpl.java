package com.gtech.Master.Persistance.Services;

import com.gtech.Master.Persistance.Entity.Category;
import com.gtech.Master.Persistance.Repository.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryDaoServiceImpl implements CategoryDaoService {

    @Autowired
    CategoryRepo categoryRepo;

    @Override
    public Category save(Category category) {
        return this.categoryRepo.save(category);
    }

    @Override
    public Optional<Category> findById(String id) {
        return this.categoryRepo.findById(id);
    }

    @Override
    public List<Category> findAllActive(Boolean active) {
        return this.categoryRepo.findByIsActive(active);
    }
}
