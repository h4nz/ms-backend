package com.gtech.Master.Persistance.Services;

import com.gtech.Master.Persistance.Entity.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryDaoService {
    Category save (Category category);
    Optional<Category> findById (String id);
    List<Category> findAllActive(Boolean active);
}
