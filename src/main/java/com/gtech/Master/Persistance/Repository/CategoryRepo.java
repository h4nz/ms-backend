package com.gtech.Master.Persistance.Repository;

import com.gtech.Master.Persistance.Entity.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo extends MongoRepository<Category,String> {
    @Query("{IsActive:?0}")
    List<Category> findByIsActive(Boolean isActive);
}
