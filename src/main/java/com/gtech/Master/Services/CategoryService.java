package com.gtech.Master.Services;

import com.gtech.Util.Exception.UserException;
import com.gtech.Master.Model.CategoryRequestDto;
import com.gtech.Master.Persistance.Entity.Category;

import java.util.List;

public interface CategoryService {
    Category addNew(CategoryRequestDto requestDto) throws UserException;
    Category edit(Category category) throws UserException;
    Category editIsActive(String id) throws UserException;
    Category editIsNotActive(String id) throws UserException;

    List<Category> getList(boolean status) throws UserException;
    Category getById(String id) throws UserException;
}
