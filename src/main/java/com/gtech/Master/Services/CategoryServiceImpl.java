package com.gtech.Master.Services;

import com.gtech.Util.Exception.ExceptionConstant;
import com.gtech.Util.Exception.InternalServerException;
import com.gtech.Util.Exception.ResourceNotFound;
import com.gtech.Util.Exception.UserException;
import com.gtech.Master.Model.CategoryRequestDto;
import com.gtech.Master.Persistance.Entity.Category;
import com.gtech.Master.Persistance.Services.CategoryDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryDaoService categoryDaoService;

    @Override
    public Category addNew(CategoryRequestDto requestDto) throws UserException {

        try {
            Category category = requestDto.converTo(Category.class);
             return this.categoryDaoService.save(category);
        }catch (Exception e){
            throw new InternalServerException(ExceptionConstant.CommonErrorCode.GENERAL_ERROR_CODE);
        }

    }

    @Override
    public Category edit(Category category) throws UserException {
        Optional<Category> categoryDb = this.categoryDaoService.findById(category.getId());
        if (categoryDb.isPresent()){
            Category categoryUpdate = categoryDb.get();
            categoryUpdate.setCodeCategory(category.getCodeCategory());
            if (!category.getIsActive().toString().isEmpty()) {
                categoryUpdate.setIsActive(category.getIsActive());
            }
            return this.categoryDaoService.save(categoryUpdate);
        }else{
            throw new ResourceNotFound(category.toString());
        }
    }

    @Override
    public Category editIsActive(String id) throws UserException {
        return null;
    }

    @Override
    public Category editIsNotActive(String id) throws UserException {
        return null;
    }

    @Override
    public List<Category> getList(boolean status) throws UserException {
        return this.categoryDaoService.findAllActive(status);
    }

    @Override
    public Category getById(String id) throws UserException {
        Optional<Category> categoryDb = this.categoryDaoService.findById(id);
        if (categoryDb.isPresent()){
            Category category = categoryDb.get();
            return category;
        }else{
            throw new ResourceNotFound(id);
        }
    }
}
