package com.gtech.Master.Controller;

import com.gtech.Util.Exception.InternalServerException;
import com.gtech.Util.Exception.UserException;
import com.gtech.Master.Model.CategoryRequestDto;
import com.gtech.Master.Persistance.Entity.Category;
import com.gtech.Master.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/master")
@CrossOrigin
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @CrossOrigin
    @RequestMapping(value = "/category",method = RequestMethod.POST)
    public ResponseEntity<Category> addNewData(@Valid @RequestBody CategoryRequestDto category) throws UserException {
        try{
            return ResponseEntity.ok().body(categoryService.addNew(category));
        }catch (UserException e){
            throw new InternalServerException(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/category",method = RequestMethod.PUT)
    public ResponseEntity<Category> editData(@RequestBody Category category) throws UserException {
        try{
            return ResponseEntity.ok().body(this.categoryService.edit(category));
        }catch (UserException e){
            throw new InternalServerException(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/category/",method = RequestMethod.GET)
    public ResponseEntity<List<Category>> getListData(@RequestParam boolean status) throws UserException {
        try{
            List<Category> categories = this.categoryService.getList(status);
            return ResponseEntity.ok().body(categories);
        }catch (UserException e){
            throw new InternalServerException(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/category/detail/",method = RequestMethod.GET)
    public ResponseEntity<Category> getById(@RequestParam String id) throws UserException {
        try{
            return ResponseEntity.ok().body(this.categoryService.getById(id));
        }catch (UserException e){
            throw new InternalServerException(e.getMessage());
        }
    }

}
