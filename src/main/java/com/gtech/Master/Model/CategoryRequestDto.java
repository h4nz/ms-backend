package com.gtech.Master.Model;

import com.gtech.Util.ConvertModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class CategoryRequestDto implements ConvertModel {

    @NotBlank
    @NotNull
    private String codeCategory;
}
