package com.gtech.Item.Persistance.Services;

import com.gtech.Item.Persistance.Entity.Item;

import java.util.List;
import java.util.Optional;

public interface ItemDaoService {
    Item save (Item item);
    Optional<Item> findById (String id);
    Optional<Item> findByBarcodeCode(String barcodeCode);
    List<Item>findAllActive(Boolean active);
}
