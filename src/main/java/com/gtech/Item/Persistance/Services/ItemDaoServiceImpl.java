package com.gtech.Item.Persistance.Services;

import com.gtech.Item.Persistance.Entity.Item;
import com.gtech.Item.Persistance.Repository.ItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ItemDaoServiceImpl implements ItemDaoService {

    @Autowired
    ItemRepo itemRepo;

    @Override
    public Item save(Item item) {
        return this.itemRepo.save(item);
    }

    @Override
    public Optional<Item> findById(String id) {
        return this.itemRepo.findById(id);
    }

    @Override
    public Optional<Item> findByBarcodeCode(String barcodeCode) {
        return this.itemRepo.finItemByBarcode(barcodeCode);
    }

    @Override
    public List<Item> findAllActive(Boolean active) {
        return this.itemRepo.findItemByIsActive(active);
    }
}
