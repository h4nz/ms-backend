package com.gtech.Item.Persistance.Repository;

import com.gtech.Item.Persistance.Entity.Item;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepo extends MongoRepository<Item, String> {

    @Query("{ 'barcodeCode'  : ?0}")
    Optional<Item> finItemByBarcode(String barcode);

    @Query("{isActive:?0}")
    List<Item> findItemByIsActive(Boolean isActive);
}
