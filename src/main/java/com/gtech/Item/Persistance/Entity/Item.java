package com.gtech.Item.Persistance.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@Document(collection = "item")
public class Item implements Serializable {
    private static final long serialVersionUID = -2692722436255640434l;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid",strategy = "uuid")
    private String id;

    private String itemCode;
    private String barcodeCode;
    private String itemName;
    private String description;
    private String category;
    private String group;
    private Date expiredDate;
    private String purchasingUnit;
    private String unitItem;
    private double purchasingPrice;
    private double sellingPrice;
    private Date purchasingDate;
    private String supplierCode;
    private double balance;
    private Boolean isActive;
}
