package com.gtech.Item.Model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ItemListResponse {
    private String id;
    private String barcodeCode;
    private String itemName;
    private String category;
    private Date expiredDate;
    private String unitItem;
    private String supplierCode;
    private double balance;
}
