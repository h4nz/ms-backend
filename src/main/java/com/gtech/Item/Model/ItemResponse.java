package com.gtech.Item.Model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ItemResponse {
    private String id;
    private String itemCode;
    private String barcodeCode;
    private String itemName;
    private String description;
    private String category;
    private String group;
    private Date expiredDate;
    private String purchasingUnit;
    private String unitItem;
    private double purchasingPrice;
    private double sellingPrice;
    private Date purchasingDate;
    private String supplierCode;
    private double balance;
    private Boolean isActive;
}
