package com.gtech.Item.Services;

import com.gtech.Item.Model.ItemListResponse;
import com.gtech.Item.Model.ItemRequest;
import com.gtech.Item.Model.ItemResponse;
import com.gtech.Util.Exception.UserException;

import java.util.List;

public interface ItemServices {
    ItemResponse addNewItem(ItemRequest itemRequest) throws UserException;
    ItemResponse editItem(ItemRequest itemRequest) throws UserException;
    ItemResponse editItemIsActive(String id) throws UserException;
    ItemResponse editItemIsNotActive(String id) throws UserException;

    List<ItemListResponse> getListItem(boolean status) throws UserException;
    ItemResponse getItemById(String id) throws UserException;
}
