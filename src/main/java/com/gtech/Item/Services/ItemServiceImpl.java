package com.gtech.Item.Services;

import com.gtech.Item.Model.ItemListResponse;
import com.gtech.Item.Model.ItemRequest;
import com.gtech.Item.Model.ItemResponse;
import com.gtech.Item.Persistance.Entity.Item;
import com.gtech.Item.Persistance.Services.ItemDaoService;
import com.gtech.Util.Exception.ExceptionConstant;
import com.gtech.Util.Exception.ResourceNotFound;
import com.gtech.Util.GenerateNumber.UniquiNumber;
import com.gtech.Util.Exception.UserException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gtech.Util.Exception.ExceptionConstant.CommonErrorCode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ItemServiceImpl implements ItemServices {

    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    ItemDaoService itemDaoService;

    @Override
    public ItemResponse addNewItem(ItemRequest itemRequest) throws UserException {
        try {
            Item item = convertToEntity(itemRequest);
            item.setBarcodeCode(UniquiNumber.generateNumber12());
            item.setIsActive(true);
            ItemResponse itemResponse = convertFromEntity(this.itemDaoService.save(item));
            return itemResponse;
        } catch (ParseException e) {
            throw new UserException(CommonErrorCode.GENERAL_ERROR_CODE, ExceptionConstant.CommonErrorDesc.get(CommonErrorCode.GENERAL_ERROR_CODE));
        }
    }

    @Override
    public ItemResponse editItem(ItemRequest itemRequest) throws UserException {
        try {
            Optional<Item> itemOptional = this.itemDaoService.findById(itemRequest.getItemId());
            if (itemOptional.isPresent()){
                Item itemDb = itemOptional.get();
                Item item = convertToEntity(itemRequest);
                item.setId(itemDb.getId());
                ItemResponse itemResponse = convertFromEntity( itemDaoService.save(item));
                return  itemResponse;
            }else{
                throw new ResourceNotFound(CommonErrorCode.SUCCESS_NO_CONTENT);
            }
        }catch (ParseException e){
            throw new UserException(CommonErrorCode.GENERAL_ERROR_CODE, ExceptionConstant.CommonErrorDesc.get(CommonErrorCode.GENERAL_ERROR_CODE));
        }
    }

    @Override
    public ItemResponse editItemIsActive(String id) throws UserException {
        try{
            Optional<Item> itemOptional = this.itemDaoService.findById(id);
            if (itemOptional.isPresent()){
                Item item = itemOptional.get();
                item.setIsActive(true);
                ItemResponse itemResponse = convertFromEntity(this.itemDaoService.save(item));
                return itemResponse;
            }else{
                throw new UserException(CommonErrorCode.SUCCESS_NO_CONTENT,ExceptionConstant.CommonErrorDesc.get(CommonErrorCode.SUCCESS_NO_CONTENT));
            }
        }catch (Exception e){
            throw new UserException(CommonErrorCode.GENERAL_ERROR_CODE, ExceptionConstant.CommonErrorDesc.get(CommonErrorCode.GENERAL_ERROR_CODE));
        }
    }

    @Override
    public ItemResponse editItemIsNotActive(String id) throws UserException {
        try{
            Optional<Item> itemOptional = this.itemDaoService.findById(id);
            if (itemOptional.isPresent()){
                Item item = itemOptional.get();
                item.setIsActive(false);
                ItemResponse itemResponse = convertFromEntity(this.itemDaoService.save(item));
                return itemResponse;
            }else{
                throw new UserException(CommonErrorCode.SUCCESS_NO_CONTENT,ExceptionConstant.CommonErrorDesc.get(CommonErrorCode.SUCCESS_NO_CONTENT));
            }
        }catch (Exception e){
            throw new UserException(CommonErrorCode.GENERAL_ERROR_CODE, ExceptionConstant.CommonErrorDesc.get(CommonErrorCode.GENERAL_ERROR_CODE));
        }
    }

    @Override
    public List<ItemListResponse> getListItem(boolean status) throws UserException {
        List<ItemListResponse>responseList = new ArrayList<ItemListResponse>();
        List<Item> itemList = this.itemDaoService.findAllActive(status);
        if (itemList.isEmpty() == false){
            responseList = itemList.stream().map(this::convertFromEntityToListItem).collect(Collectors.toList());
        }
        return responseList;
    }

    @Override
    public ItemResponse getItemById(String id) throws UserException {
        Optional<Item> itemOptional = this.itemDaoService.findById(id);
        if(itemOptional.isPresent()){
            Item item = itemOptional.get();
            item.setIsActive(true);
            ItemResponse itemResponse = convertFromEntity(this.itemDaoService.save(item));
            return itemResponse;
        }else {
            throw new ResourceNotFound(CommonErrorCode.SUCCESS_NO_CONTENT);
        }
    }

    private Item convertToEntity(Object object ) throws ParseException {
        Item item = modelMapper.map(object, Item.class);
        return item;
    }

    private ItemResponse convertFromEntity(Item item){
        ItemResponse itemResponse = modelMapper.map(item,ItemResponse.class);
        return itemResponse;
    }

    private ItemListResponse convertFromEntityToListItem(Item item){
        ItemListResponse listResponse = modelMapper.map(item,ItemListResponse.class);
        return listResponse;
    }
}
