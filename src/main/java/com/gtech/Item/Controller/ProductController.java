package com.gtech.Item.Controller;

import com.gtech.Item.Model.ItemListResponse;
import com.gtech.Item.Model.ItemRequest;
import com.gtech.Item.Model.ItemResponse;
import com.gtech.Item.Services.ItemServices;
import com.gtech.Util.Exception.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/product")
@CrossOrigin
public class ProductController {

    @Autowired
    ItemServices itemServices;

    @CrossOrigin
    @RequestMapping(value = "/products/master",method = RequestMethod.POST)
    public ResponseEntity<ItemResponse> addNewItem(@RequestBody ItemRequest itemRequest) throws UserException {
        try{
            ItemResponse itemResponse = itemServices.addNewItem(itemRequest);
            return ResponseEntity.ok().body(itemResponse);
        }catch (UserException e){
            throw new UserException(e.getErrorCode(),e.getErrorDesc());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/products/master",method = RequestMethod.PUT)
    public ResponseEntity<ItemResponse> editItem(@RequestBody ItemRequest itemRequest) throws UserException {
        try{
            ItemResponse itemResponse = itemServices.editItem(itemRequest);
            return ResponseEntity.ok().body(itemResponse);
        }catch (UserException e){
            throw new UserException(e.getErrorCode(),e.getErrorDesc());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/products/isStatus",method = RequestMethod.POST)
    public ResponseEntity<ItemResponse> editStatus(@RequestParam String id,@RequestParam boolean status) throws UserException {
        ItemResponse itemResponse = new ItemResponse();
        try{
            if (status){
                itemResponse = itemServices.editItemIsActive(id);
            }else{
                 itemResponse = itemServices.editItemIsNotActive(id);
            }
            return ResponseEntity.ok().body(itemResponse);
        }catch (UserException e){
            throw new UserException(e.getErrorCode(),e.getErrorDesc());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/products/",method = RequestMethod.GET)
    public ResponseEntity<List<ItemListResponse>> getListsProduct(@RequestParam boolean status) throws UserException {
        try{
            List<ItemListResponse> responseList = itemServices.getListItem(status);
            return ResponseEntity.ok().body(responseList);
        }catch (UserException e){
            throw new UserException(e.getErrorCode(),e.getErrorDesc());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/products/detail/",method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> getProdutById(@RequestParam String id) throws UserException {
        ItemResponse itemResponse = itemServices.getItemById(id);
        return ResponseEntity.ok().body(itemResponse);
    }

}
