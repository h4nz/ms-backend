package com.gtech.Master.Services;

import static org.assertj.core.api.Assertions.assertThat;

import com.gtech.Master.Model.CategoryRequestDto;
import com.gtech.Master.Persistance.Entity.Category;
import com.gtech.Master.Persistance.Services.CategoryDaoService;
import com.gtech.Util.Exception.UserException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CategoryTest {

    @Mock
    CategoryDaoService categoryDaoService;


    @InjectMocks
    CategoryService categoryService = new CategoryServiceImpl();

    @Test
    void addNewCategory_shouldReturnCateogry_whenAddNewCategoryInvoked() throws UserException {
        CategoryRequestDto requestDto = new CategoryRequestDto();
        requestDto.setCodeCategory("codeCategory");
        Category category = new Category();
        category.setCodeCategory("codeCategory");
        category.setIsActive(true);

        Mockito.when(this.categoryDaoService.save(category)).thenReturn(category);

        Category result =  this.categoryService.addNew(requestDto);
        assertThat(result).isEqualTo(category);
    }

    @Test
    void editCategory_shouldReturnCateogry_whenAddNewCategoryInvoked() throws UserException {
        Category category = new Category();
        category.setId("id");
        category.setCodeCategory("codeCategory");
        category.setIsActive(true);

        Category category2 = new Category();
        category2.setId("id");
        category2.setCodeCategory("codeCategory");
        category2.setIsActive(false);

        Mockito.when(this.categoryDaoService.findById(category.getId())).thenReturn(java.util.Optional.of(category));
        Mockito.when(this.categoryDaoService.save(category)).thenReturn(category2);

        Category result = this.categoryService.edit(category);
        assertThat(result).isEqualTo(category2);
    }

    @Test
    void getListCategory_shouldReturnCateogry_whenAddNewCategoryInvoked() throws UserException {
        List<Category> categoryList = new ArrayList<>();
        Category category = new Category();
        category.setId("id");
        category.setCodeCategory("codeCategory");
        category.setIsActive(true);
        categoryList.add(category);

        Mockito.when(this.categoryDaoService.findAllActive(true)).thenReturn(categoryList);

        List<Category> result =  this.categoryService.getList(true);
        assertThat(result).isEqualTo(categoryList);
    }

    @Test
    void getCategoryById_shouldReturnCateogry_whenAddNewCategoryInvoked() throws UserException {
        Category category = new Category();
        category.setId("id");
        category.setCodeCategory("codeCategory");
        category.setIsActive(true);

        Mockito.when(this.categoryDaoService.findById("id")).thenReturn(java.util.Optional.of(category));

        Category result =  this.categoryService.getById("id");
        assertThat(result).isEqualTo(category);
    }

}

